const express = require('express');
const passport = require('./src/passport.js');
const jwt = require('jsonwebtoken');
const express_jwt = require('express-jwt');
const config = require('./config.js');
const path = require('path');
const bodyParser = require('body-parser');
const authenticate = express_jwt({secret : config.secret});
const cookieParser = require('cookie-parser');

var app = express();
app.use(passport.initialize());
app.use(cookieParser());
app.use(bodyParser.urlencoded({
        extended : true
}));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, '/public')));
app.use('/botchat',auth, express.static(path.join(__dirname, '/webchat')));

app.post('/login', passport.authenticate('local', 
    {
        session: false
    }),
    serialize,
    generate_token,
    respond
);

function auth(req, res, next) {
    console.log('checking cookies');
    jwt.verify(req.cookies.token, config.secret, (err, decoded) => {
        if(err){
            res.status(403).send({message : 'access forbidden'});
        }
        else{
            next();
        }
    })
}

function serialize(req,res, next){
    // update or create user for future use
    console.log('serialize');
    next();
}

function generate_token(req, res, next) {
    console.log('generate token');
    req.token = jwt.sign({
        id: req.user.id
    }, config.secret, {
        expiresIn: "2 days"
    });
    next();
}

function respond(req, res, token) {
    console.log('respond');
    res.status(200).json({
        user : req.user,
        token: req.token
    });
}
module.exports =  app;
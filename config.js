var config = {};

config.select = 'Select';
config.ongoing = {
    'variety' : 'Variety',
    'trial' : 'Trial',
    'set_forget' : 'Set it & Forget it'
};
config.prefs = {
    'Joy of Food' : 'joy_of_food',
    'Food as Fuel': 'food_as_fuel',
    'Feeding Families': 'feeding_families',
    'Solution Seekers': 'solution_seekers'
};

config.res = {
    'nutri' : 'No problem. Here is the nutrition facts for %s',
    'ingredient' : 'Sure. Here is the ingredient list for %s',
    'replace_yes' : 'Great. We will keep the '
};

config.usernames = {
    'jbill0' : true,
    'cgorthy1' : true,
    'demouser2' : true,
    'gbriggs3' : true,
    'pcoate4' : true,
    'rdicesare5' : true,
    'bmacdonald6' : true,


}
config.passwords = {
    'jbill0' : '*_galahad2018_JEREMIAH_*',
    'cgorthy1' : '*_galahad2018_CHERYL_*',
    'demouser2' : '*_galahad2018_DEMO_*',
    'gbriggs3' : '*_galahad2018_GARY_*',
    'pcoate4' : '*_galahad2018_PAT_*',
    'rdicesare5' : '*_galahad2018_ROB_*',
    'bmacdonald6' : '*_galahad2018_BOB_*',
    
};
config.password = 'hey'
config.secret = 'server'
module.exports = config;
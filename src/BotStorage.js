const azure = require("botbuilder-azure");
const TABLE_NAME = process.env.BOT_STORAGE_TABLE;
var azureTableClient = new azure.AzureTableClient(TABLE_NAME, process.env.AZURE_STORAGE_ACCOUNT, process.env.AZURE_STORAGE_ACCESS_KEY);
var botStorage = new azure.AzureBotStorage({gzipData: false}, azureTableClient);
module.exports = botStorage;
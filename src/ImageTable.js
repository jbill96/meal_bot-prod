const azureStorage = require('azure-storage');
require('dotenv-extended').load();
const blobService = azureStorage.createBlobService(process.env.AZURE_STORAGE_ACCOUNT, process.env.AZURE_STORAGE_ACCESS_KEY);
const CONTAINER = 'mealblobs';

function getImageUrl(filename) {
    var startDate = new Date();
    var expiryDate = new Date(startDate);
    expiryDate.setMinutes(startDate.getMinutes() + 100);
    startDate.setMinutes(startDate.getMinutes() - 100);

    var sharedAccessPolicy = {
        AccessPolicy: {
            Permissions: azureStorage.BlobUtilities.SharedAccessPermissions.READ,
            Start: startDate,
            Expiry: expiryDate
        }
    };

    var token = blobService.generateSharedAccessSignature(CONTAINER, filename, sharedAccessPolicy);
    var url = blobService.getUrl(CONTAINER, filename, token);
    return url;
}
module.exports = {
    getImageUrl: getImageUrl
};
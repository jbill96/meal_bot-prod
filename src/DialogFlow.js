"use strict";
const apiai = require('apiai-promise');
const uuid = require('uuid');
const translator = require('./translator.js');
const async = require('async');
const config = require('../config.js');
const logger = require('./log.js');
const Time = require('time-diff');

var ApiAiRecognizer = function (token) {
    this.app = apiai(token);
    this._onEnabled = [];
    this._onFilter = [];
};

ApiAiRecognizer.prototype.recognize = function (context, done) {
    var _this = this;

    this.isEnabled(context, function (err, enabled) {
        if (err) {
            callback(err, null);
        } else if (!enabled) {
            callback(null, {
                score: 0.0,
                intent: null
            });
        } else {
            var intent = {
                score: 0.0
            };
            try {
                var sessionId = context.message.address.user.id + context.message.address.channelId;
                if (sessionId.length > 36) {
                    sessionId = sessionId.slice(0, 35);
                }
            } catch (err) {
                var sessionId = uuid();
            }
            if (context.message.text && !context.message.value) {
                var middlewareTime = new Time();
                middlewareTime.start('middleware');
                logger.info({
                    file: 'DialogFlow.js',
                    message : 'beginning message middleware'
                });
                async.waterfall([
                    function (callback) {
                        logger.info({
                            file: 'DialogFlow.js',
                            message : 'detecting language'
                        });
                        translator.detect(context.message.text, callback);
                    },
                    function (lang, callback) {
                        intent.from = lang;
                        if (lang !== 'en') {
                            logger.info({
                                file: 'DialogFlow.js',
                                message : 'detected non-english language',
                                lang : lang
                            });
                            translator.translate({
                                text: context.message.text,
                                from: lang,
                                to: 'en'

                            }, callback);
                        } else {
                            callback(null, context.message.text, lang);
                        }
                    },
                    function (message, lang, callback) {
                        logger.info({
                            file: 'DialogFlow.js',
                            message : 'sending request to dialogflow'
                        });
                        var dialogTime = new Time();
                        dialogTime.start('dialogflow request');
                        _this.app.textRequest(message.toLowerCase(), {
                                sessionId: sessionId
                            })
                            .then((response) => {
                                var result = response.result;
                                logger.info({
                                    file : 'DialogFlow.js',
                                    message : 'recieved response from dialogflow',
                                    request : {
                                        duration : dialogTime.end('dialogflow request'),
                                    }
                                });
                                
                                /*
                                console.log('********************************');
                                logger.info({
                                    file : 'apiai.js', 
                                    message : 'logging result',
                                    result : result
                                });
                                console.log('********************************');
                                logger.info({
                                    file : 'apiai.js', 
                                    message : 'logging result parameters',
                                    parameters : result.parameters
                                });
                                console.log('********************************');
                                
                                logger.info({
                                    file : 'apiai.js', 
                                    message : 'logging message response',
                                    messages : result.fulfillment.messages
                                });
                                console.log('********************************');
                                
                                */
                                intent = handle_apiai_result(intent, result);
                                intent.contexts = result.contexts;
                                callback(null, intent, lang, context.userData.lang);
                            }).catch((err) => {
                                logger.error({
                                    file : 'DialogFlow.js',
                                    message : 'recieved error from dialogflow',
                                    request : {
                                        duration : dialogTime.end('dialogflow request'),
                                    },
                                    error : err
                                });
                            });
                    },
                    final_translation
                ], (err, results) => {
                    if (err) {
                        logger.info({
                            file : 'DialogFlow.js',
                            message : 'error at end of middleware',
                            request : {
                                duration : middlewareTime.end('middleware'),
                            },
                            error : err
                        });
                        done(err);
                    }
                    logger.info({
                        file : 'DialogFlow.js',
                        message : 'finished middleware, returning result to user',
                        request : {
                            duration : middlewareTime.end('middleware'),
                        }
                    });
                    done(null, results);
                });
            } else if (context.message.value) {
                var value = context.message.value;
                console.log(value);
                if (value.pick_list) {
                    console.log('submitting pick list');
                    var items_obj = context.message.value.pick_list;
                    var items_arr = items_obj.split(';').map((val) => {
                        return val;
                    });
                    var config_lang = context.userData.lang === 'en' ? config.en : config.ch;
                    async.waterfall([
                        (callback) => {
                            _this.app.textRequest(items_obj, {
                                sessionId : sessionId
                            })
                            .then((response) => {
                                var result = response.result;
                                intent = handle_apiai_result(intent, result);
                                intent.config_lang = config_lang;
                                intent.parameters = {
                                    pick_list : items_arr
                                };
                                callback(null, intent, value.lang, context.userData.lang);
                            }).catch((err) => {
                                callback(err);
                            });
                        },
                        final_translation
                    ], (err, results) => {
                        done(null, results);
                    });
                } 
            } else {
                intent = {
                    score: 1,
                    intent: "None",
                    entities: []
                };
                done(null, intent);
            }
        }
    });
}

function get_messages(messages) {
    var messages_list = [];
    messages.forEach((msg) => {
        if(msg){
            messages_list.push(msg.speech);         
        }
    });
    return messages_list;
}

function handle_apiai_result(intent, result) {
    if (result.source == 'domains') {
        entities_found = [{
                entity: result.fulfillment.speech,
                type: 'fulfillment',
                score: 1
            },
            {
                entity: result.actionIncomplete,
                type: 'actionIncomplete',
                score: 1
            }
        ];
        intent = {
            score: result.score,
            intent: result.source,
            entities: entities_found,
        };
    } else if (result.source == 'agent') {
        var entities_found = [{
                entity: result.fulfillment.speech,
                messages: result.fulfillment.messages,
                type: 'fulfillment',
                score: 1
            },
            {
                entity: result.actionIncomplete,
                type: 'actionIncomplete',
                score: 1
            }
        ];
        intent = {
            score: result.score,
            intent: result.metadata.intentName,
            entities: entities_found,
            parameters: result.parameters,
        };
    }
    intent.messages = get_messages(result.fulfillment.messages);
    return intent;
}

function final_translation(intent, lang, user_lang, callback) {
    if (lang !== 'en' && lang == 'zh-CHS') {
        intent.lang = lang;
        intent.config_lang = config.ch;
        translator.translate_array(intent, lang, callback);
    } else {
        if (user_lang === 'zh-CHS') {
            intent.lang = user_lang;
            intent.config_lang = config.ch;
            callback(null, intent);
        } else {
            intent.lang = 'en';
            intent.config_lang = config.en;
            callback(null, intent);
        }
    }
}

ApiAiRecognizer.prototype.onEnabled = function (handler) {
    this._onEnabled.unshift(handler);
    return this;
};

ApiAiRecognizer.prototype.onFilter = function (handler) {
    this._onFilter.push(handler);
    return this;
};

ApiAiRecognizer.prototype.isEnabled = function (context, callback) {
    var index = 0;
    var _that = this;

    function next(err, enabled) {
        if (index < _that._onEnabled.length && !err && enabled) {
            try {
                _that._onEnabled[index++](context, next);
            } catch (e) {
                callback(e, false);
            }
        } else {
            callback(err, enabled);
        }
    }
    next(null, true);
};

ApiAiRecognizer.prototype.filter = function (context, result, callback) {
    var index = 0;
    var _that = this;

    function next(err, r) {
        if (index < _that._onFilter.length && !err) {
            try {
                _that._onFilter[index++](context, r, next);
            } catch (e) {
                callback(e, null);
            }
        } else {
            callback(err, r);
        }
    }
    next(null, result);
};

module.exports = ApiAiRecognizer;
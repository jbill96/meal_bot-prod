const passport = require('passport');
const Strategy = require('passport-local');
const bcrypt = require('bcrypt');
const config = require('../config.js');
const saltRounds = 10;

passport.use(new Strategy((username, password, done) => {
    var salt = bcrypt.genSaltSync(saltRounds);
    var hash = bcrypt.hashSync(password, salt);
    if (config.usernames[username] && password == config.passwords[username]) {
        done(null, {
            id: 'galahad_admin',
            username: username,
            password: hash,
            email: 'admin@galahad.com',
            verified: true
        });
    } else {
        done(null, false);
    }
}));

module.exports = passport;